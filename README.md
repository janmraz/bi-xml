# BI-XML semestrální práce

## Příkazy

### Spojení .xml souborů do jednoho souboru

`xmllint --dropdtd --noent xml/countries/join-countries.xml > xml/countries.xml`

### DTD validace

Úspěšná validace nevypisuje žádný text

`xmllint --noout --dtdvalid xml/schema.dtd xml/countries.xml`

### RelaxNG

#### Konverze z kompaktní syntaxe do XML syntaxe

`trang -I rnc -O rng 'xml/schema.rnc' 'xml/schema.rng'`

#### Validace oproti schématu v kompaktní syntaxi

Úspěšná validace nevypisuje žádný text

`jing -c 'xml/schema.rnc' 'xml/countries.xml'`

#### Validace oproti schématu v XML syntaxi pomocí xmllint

Úspěšná validace vypisuje `xml/countries.xml validates`

`xmllint --noout --relaxng xml/schema.rng xml/countries.xml`

#### Validace oproti schématu v XML syntaxi pomocí jing

Úspěšná validace nevypisuje žádný text

`jing 'xml/schema.rng' 'xml/countries.xml'`

### Transformace do HTML

`java -jar saxon9he.jar xml/countries.xml html/html-transformation.xslt > html/index.html`

### Výstup dat do PDF
`fop -xml xml/countries.xml -xsl pdf/pdf-transformation.xslt -pdf pdf/pdf-output.pdf`

### Zdroj CSS stylů
https://github.com/dhg/Skeleton
