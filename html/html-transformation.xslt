<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <head>
    <meta charset="utf-8" />
    <title>BI-XML</title>
    <link href="//fonts.googleapis.com/css?family=Raleway:400,300,600"
rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../resources/skeleton.css" />
    <style>
    .holder {
        margin-top: 0;
        margin-left: 0.5rem;
        margin-bottom: 2rem;
    }
    h1 {
       text-align: center;
    }
    a {
        text-decoration: none;
    }
    </style>
  </head>
  <body>
    <h1>BI-XML project</h1>
    <hr/>
    <xsl:apply-templates mode="menu" />
    <hr/>
    <xsl:apply-templates />
  </body>
  </html>
</xsl:template>

<xsl:template match="countries">
    <div class="container">
            <xsl:apply-templates />
    </div>
</xsl:template>

<xsl:template match="countries" mode="menu">
  <header>
  <div class="row">
        <xsl:for-each select="country">
                <a class="three columns button"  style="text-align: center;">
                    <xsl:attribute name="href">
                        <xsl:text>#</xsl:text>
                        <xsl:value-of select="@name" />
                    </xsl:attribute>
                    <xsl:value-of select="@name" />
                </a>
        </xsl:for-each>
  </div>
</header>
</xsl:template>

<xsl:template match="country">
    <div>
        <h2>
                <xsl:value-of select="@name" />
        </h2>
        <div>
            <xsl:attribute name="id">
                <xsl:value-of select="@name" />
            </xsl:attribute>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="images">
    <div>
            <xsl:apply-templates />
    </div>
</xsl:template>

<xsl:template match="image">
    <img>
        <xsl:attribute name="src">
           <xsl:text>../resources/</xsl:text>
           <xsl:value-of select="."/>
        </xsl:attribute>
    </img>
</xsl:template>

<xsl:template match="introduction">
    <div >
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:value-of select="background" />
        </div>
    </div>
</xsl:template>

<xsl:template match="geography">
    <div >
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="location">
    <div >
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="geographic-coordinates">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="latitude/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="latitude" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="latitude/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="longitude/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="longitude" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="longitude/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="area">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="water/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="water" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="water/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="land-boundaries">
<div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
        </div>
        <h6><xsl:value-of select="border-countries/@name" /></h6>
        <ul class="holder">
        <xsl:for-each select="border-countries/border-country">
            <li>
                <xsl:value-of select="@name"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="@unit"/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="coastline">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="maritime-claims">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="territorial-sea/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="territorial-sea" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="territorial-sea/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="exclusive-economic-zone/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exclusive-economic-zone" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exclusive-economic-zone/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="continental-shelf/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="continental-shelf" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="continental-shelf-shelfer/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="climate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="terrain">
        <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="elevation">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="mean-elevation/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mean-elevation" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mean-elevation/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="lowest-point/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-point/place" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-point/height/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="highest-point/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-point/place" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-point/height/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="land-use">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="agricultural-land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agricultural-land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agricultural-land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="arable-land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="arable-land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="arable-land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="permanent-crops/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-crops" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-crops/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="permanent-pasture/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-pasture" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-pasture/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="forest/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="forest" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="forest/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="other/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="other" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="other/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="natural-resources">
    <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="resource">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="irrigated-land">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="natural-hazards">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="environment">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="current-issues">
        <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="international-agreements">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <div>
                <h6><xsl:value-of select="party-to/@name" /></h6>
                <ul class="holder">
                <xsl:for-each select="party-to/agreement">
                    <li>
                        <xsl:value-of select="."/>
                    </li>
                </xsl:for-each>
                </ul>
            </div>
            <div>
                <h6><xsl:value-of select="signed-but-not-ratified/@name" /></h6>
                <ul class="holder">
                <xsl:for-each select="signed-but-not-ratified/agreement">
                    <li>
                        <xsl:value-of select="."/>
                    </li>
                </xsl:for-each>
                </ul>
            </div>
        </div>
    </div>
</xsl:template>

<xsl:template match="people-and-society">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="population">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="nationality">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="noun/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="noun" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="adjective/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adjective" />
        </div>
    </div>
</xsl:template>

<xsl:template match="population-distribution">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="ethnic-groups">
    <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="group">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="languages">
    <div>
        <h4><xsl:value-of select="@name" /></h4>
        <h5><xsl:value-of select="official/@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="official/language">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
        <h5><xsl:value-of select="unofficial/@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="unofficial/language">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="religions">
    <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="religion">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="age-structure">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div>
            <ul class="holder">
                <li>
                  <xsl:value-of select="years-0-14/@name" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-0-14" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-0-14/@unit" />
               </li>
               <li>
                  <xsl:value-of select="years-15-24/@name" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-15-24" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-15-24/@unit" />
               </li>
               <li>
                  <xsl:value-of select="years-25-54/@name" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-25-54" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-25-54/@unit" />
               </li>
               <li>
                  <xsl:value-of select="years-55-64/@name" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-55-64" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-55-64/@unit" />
               </li>
               <li>
                  <xsl:value-of select="years-65/@name" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-65" />
                  <xsl:text> </xsl:text>
                  <xsl:value-of select="years-65/@unit" />
               </li>
            </ul>
        </div>
    </div>
</xsl:template>

<xsl:template match="dependency-ratios">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="youth-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="youth-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elderly-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elderly-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="potential-support-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="potential-support-ratio" />
        </div>
    </div>
</xsl:template>

<xsl:template match="median-age">
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </div>
</xsl:template>

<xsl:template match="population-growth-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="birth-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="death-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="net-migration-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="urbanization">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="urban-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="urban-population" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="urban-population/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="rate-of-urbanization/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="rate-of-urbanization" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="rate-of-urbanization/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="major-urban-areas-population">
    <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="area">
            <li>
                <xsl:value-of select="@name"/>
                <xsl:text> - </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="@unit" />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="sex-ratio">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <ul class="holder">
            <li>
              <xsl:value-of select="at-birth/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="at-birth" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="at-birth/@unit" />
            </li>
            <li>
              <xsl:value-of select="years-0-14/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-0-14" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-0-14/@unit" />
           </li>
           <li>
              <xsl:value-of select="years-15-24/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-15-24" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-15-24/@unit" />
           </li>
           <li>
              <xsl:value-of select="years-25-54/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-25-54" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-25-54/@unit" />
           </li>
           <li>
              <xsl:value-of select="years-55-64/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-55-64" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-55-64/@unit" />
           </li>
           <li>
              <xsl:value-of select="years-65/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-65" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="years-65/@unit" />
           </li>
           <li>
              <xsl:value-of select="total-population/@name" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="total-population" />
              <xsl:text> </xsl:text>
              <xsl:value-of select="total-population/@unit" />
           </li>
        </ul>
    </div>
</xsl:template>

<xsl:template match="mothers-mean-age-at-first-birth">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="maternal-mortality-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="infant-mortality-rate">
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </div>
</xsl:template>

<xsl:template match="life-expectancy-at-birth">
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-population" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-population/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </div>
</xsl:template>

<xsl:template match="total-fertility-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="health-expenditures">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="physicians-density">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="hospital-bed-density">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="drinking-water-source">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="improved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="improved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total/@unit" />
        </div>
        <div class="holder">
            <xsl:value-of select="unimproved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unimproved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="sanitation-facility-access">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="improved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="improved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total/@unit" />
        </div>
        <div class="holder">
            <xsl:value-of select="unimproved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unimproved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="HIV-AIDS">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="adult-prevalence-rate/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adult-prevalence-rate" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adult-prevalence-rate/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="deaths/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="deaths" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="deaths/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="education-expenditures">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="obesity-adult-prevalence-rate">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="school-life-expectancy">
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </div>
</xsl:template>

<xsl:template match="unemployment-youth-ages-15-24">
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </div>
</xsl:template>

<xsl:template match="government">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="country-name">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="conventional-long-form/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="conventional-long-form" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="conventional-short-form/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="conventional-short-form" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="etymology/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="etymology" />
        </div>
    </div>
</xsl:template>

<xsl:template match="government-type">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="capital">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="name" />
        </div>
    </div>
</xsl:template>

<xsl:template match="independence">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="national-holiday">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="constitution">
  <div>
      <h5>
          <xsl:value-of select="@name" />
      </h5>
      <div class="holder">
          <h6><xsl:value-of select="history/@name" /></h6>
          <xsl:value-of select="history" />
      </div>
      <div class="holder">
          <h6><xsl:value-of select="amendments/@name" /></h6>
          <xsl:value-of select="amendments" />
      </div>

  </div>
</xsl:template>

<xsl:template match="legal-system">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="international-law-organization-participation">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="citizenship">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="citizenship-by-birth/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="citizenship-by-birth" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="citizenship-by-descent-only/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="citizenship-by-descent-only" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="dual-citizenship-recognized/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="dual-citizenship-recognized" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="residency-requirement-for-naturalization/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="residency-requirement-for-naturalization" />
        </div>
    </div>
</xsl:template>

<xsl:template match="suffrage">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="executive-branch">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="chief-of-state/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="chief-of-state" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="head-of-government/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="head-of-government" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="cabinet/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="cabinet" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elections-appointments/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elections-appointments" />
        </div>
    </div>
</xsl:template>

<xsl:template match="legislative-branch">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="description/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="description" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elections/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elections" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="election-results/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="election-results" />
        </div>
    </div>
</xsl:template>

<xsl:template match="judicial-branch">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="highest-courts/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-courts" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="judge-selection-and-term-of-office/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="judge-selection-and-term-of-office" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="subordinate-courts/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="subordinate-courts" />
        </div>
    </div>
</xsl:template>

<xsl:template match="political-parties">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="party">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="diplomatic-representation-in-the-US">
  <div>
      <h5>
          <xsl:value-of select="@name" />
      </h5>
      <div class="holder">
          <xsl:value-of select="chief-of-mission/@name" />
          <xsl:text> - </xsl:text>
          <xsl:value-of select="chief-of-mission" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="chancery/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="chancery" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="telephone/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="telephone" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="FAX/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="FAX" />
      </div>
      <h6><xsl:value-of select="consulates-general/@name" /></h6>
      <ul class="holder">
      <xsl:for-each select="consulates-general/consulate">
          <li>
              <xsl:value-of select="."/>
          </li>
      </xsl:for-each>
      </ul>
  </div>
</xsl:template>


<xsl:template match="diplomatic-representation-from-the-US">
  <div>
      <h5>
          <xsl:value-of select="@name" />
      </h5>
      <div class="holder">
          <xsl:value-of select="chief-of-mission/@name" />
          <xsl:text> - </xsl:text>
          <xsl:value-of select="chief-of-mission" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="embassy/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="embassy" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="telephone/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="telephone" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="FAX/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="FAX" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="mailing-address/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="mailing-address" />
      </div>
  </div>
</xsl:template>

<xsl:template match="flag-description">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="national-symbols">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="symbol">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="national-colors">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="color">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="national-anthem">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="name/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="name" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="lyrics-music/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lyrics-music" />
        </div>
    </div>
</xsl:template>

<xsl:template match="economy">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>



<xsl:template match="economy-overview">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
          <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="gdp">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="purchasing-power-parity">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="official-exchange-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="real-growth-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="per-capita">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="gross-national-saving">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="composition-by-end-use">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="household-consumption/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="household-consumption" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="household-consumption/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="government-consumption/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="government-consumption" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="government-consumption/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="investment-in-inventories/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-inventories" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-inventories/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="composition-by-sector-of-origin">
  <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="agriculture/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agriculture" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agriculture/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="industry/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="industry" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="industry/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="services/@unit" />
          </div>
    </div>
</xsl:template>

<xsl:template match="agriculture-products">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="product">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="industries">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="industry">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="industrial-production-growth-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="labor-force">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="unemployment-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="population-below-poverty-line">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="household-income-or-consumption-by-percentage-share">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="lowest-10/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="lowest-10" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-10/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="highest-10/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="highest-10" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-10/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="distribution-of-family-income-Gini-index">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="budget">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="revenues/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="revenues" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="revenues/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="expenditures/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="expenditures" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="expenditures/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="taxes-and-other-revenues">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="budget-surplus-or-deficit">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="public-debt">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="fiscal-year">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="inflation-rate-consumer-prices">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="central-bank-discount-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="commercial-bank-prime-lending-rate">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="stock-of-narrow-money">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="stock-of-broad-money">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="stock-of-domestic-credit">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="market-value-of-publicly-traded-shares">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="current-account-balance">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="exports">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="export-partners">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="partner">
            <li>
                <xsl:value-of select="@name" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="." />
                <xsl:text> </xsl:text>
                <xsl:value-of select="@unit" />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="export-commodities">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="commodity">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="imports">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="import-commodities">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="commodity">
            <li>
                <xsl:value-of select="."/>
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="import-partners">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="partner">
            <li>
                <xsl:value-of select="@name" />
                <xsl:text> </xsl:text>
                <xsl:value-of select="." />
                <xsl:text> </xsl:text>
                <xsl:value-of select="@unit" />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="reserves-of-foreign-exchange-and-gold">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="debt-external">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="stock-of-direct-foreign-investment">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="at-home/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="at-home" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="at-home/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="abroad/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="abroad" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="abroad/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="exchange-rates">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="energy">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="electricity">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="access">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="production">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="consumption">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="installed-generating-capacity">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="from-fossil-fuels">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="from-nuclear-fuels">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="from-hydroelectric-plants">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="from-other-renewable-sources">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="crude-oil">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="refined-petroleum-products">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="natural-gas">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="proved-reserves">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="emissions">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div class="holder">
          <xsl:value-of select="." />
          <xsl:text> </xsl:text>
          <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="communications">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="telephones">
    <div>
        <h4>
            <xsl:value-of select="@name" />
        </h4>
        <div class="holder">
            <xsl:value-of select="fixed-lines/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="fixed-lines/total-subscriptions/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="fixed-lines/total-subscriptions" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="fixed-lines/subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="fixed-lines/subscriptions-per-100-inhabitants" />
        </div>
        <div class="holder">
            <xsl:value-of select="mobile-cellular/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="mobile-cellular/total-subscriptions/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mobile-cellular/total-subscriptions" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="mobile-cellular/subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mobile-cellular/subscriptions-per-100-inhabitants" />
       </div>
       <div class="holder">
            <xsl:value-of select="telephone-system/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="telephone-system/general-assessment/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/general-assessment" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="telephone-system/domestic/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/domestic" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="telephone-system/international/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/international" />
        </div>
    </div>
</xsl:template>

<xsl:template match="broadcast-media">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="internet-country-code">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="internet-users">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="percent-of-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="percent-of-population" />
        </div>
    </div>
</xsl:template>

<xsl:template match="broadband-fixed-subscriptions">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="subscriptions-per-100-inhabitants" />
        </div>
    </div>
</xsl:template>

<xsl:template match="transportation">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="national-air-transport-system">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="number-of-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="number-of-registered-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="inventory-of-registered-aircraft-operated-by-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="inventory-of-registered-aircraft-operated-by-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="annual-passenger-traffic-on-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-passenger-traffic-on-registered-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="civil-aircraft-registration-country-code-prefix">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="airports">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="airports-with-paved-runways">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-over-3047/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-over-3047" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-2438-to-3047/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-2438-to-3047" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-1524-to-2437/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-1524-to-2437" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-914-to-1523/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-914-to-1523" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-under-914/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-under-914" />
        </div>
    </div>
</xsl:template>

<xsl:template match="airports-with-unpaved-runways">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-1524-to-2437/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-1524-to-2437" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-914-to-1523/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-914-to-1523" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-under-914/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-under-914" />
        </div>
    </div>
</xsl:template>

<xsl:template match="heliports">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="pipelines">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="railways">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="standard-gauge/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="standard-gauge" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="standard-gauge/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="roadways">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="paved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="paved" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="paved/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unpaved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unpaved" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unpaved/@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="waterways">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="merchant-marine">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="total" />
        </div>
    </div>
</xsl:template>

<xsl:template match="ports-and-terminals">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="major-seaports">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="seaport">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="oil-terminals">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="terminal">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="container-ports">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="port">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="LNG-terminals">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="terminal">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="river-and-lake-ports">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="port">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="dry-bulk-cargo-ports">
      <div>
        <h5><xsl:value-of select="@name" /></h5>
        <ul class="holder">
        <xsl:for-each select="port">
            <li>
                <xsl:value-of select="." />
            </li>
        </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="military-and-security">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="military-expenditures">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </div>
    </div>
</xsl:template>

<xsl:template match="military-branches">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <h6><xsl:value-of select="forces" /></h6>
            <ul class="holder">
            <xsl:for-each select="forces/force">
                <li>
                    <xsl:value-of select="." />
                </li>
            </xsl:for-each>
            </ul>
        </div>
    </div>
</xsl:template>

<xsl:template match="service-age-and-obligation">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="transnational-issues">
    <div>
        <h3>
            <xsl:value-of select="@name" />
        </h3>
        <div>
            <xsl:apply-templates />
        </div>
    </div>
</xsl:template>

<xsl:template match="international-disputes">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

<xsl:template match="refugees-and-internally-displaced-persons">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <h6><xsl:value-of select="refugees/@name" /></h6>
            <ul class="holder">
            <xsl:for-each select="refugees/origin-country">
                <li>
                    <xsl:value-of select="@name" />
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="." />
                </li>
            </xsl:for-each>
            </ul>
        </div>
    </div>
</xsl:template>

<xsl:template match="illicit-drugs">
    <div>
        <h5>
            <xsl:value-of select="@name" />
        </h5>
        <div class="holder">
            <xsl:value-of select="." />
        </div>
    </div>
</xsl:template>

</xsl:stylesheet>
