<?xml version="1.0" encoding="UTF-8"?>

<country id="c02" name="Finland">
	<images>
		<image name="Flag">fi-flag.gif</image>
		<image name="Map">fi-map.gif</image>
	</images>
	<introduction name="Introduction">
		<background name="Background">Finland was a province and then a grand duchy under Sweden from the 12th to the 19th centuries, and an autonomous grand duchy of Russia after 1809. It gained complete independence in 1917. During World War II, Finland successfully defended its independence through cooperation with Germany and resisted subsequent invasions by the Soviet Union - albeit with some loss of territory. In the subsequent half century, Finland transformed from a farm/forest economy to a diversified modern industrial economy; per capita income is among the highest in Western Europe. A member of the EU since 1995, Finland was the only Nordic state to join the euro single currency at its initiation in January 1999. In the 21st century, the key features of Finland's modern welfare state are high quality education, promotion of equality, and a national social welfare system - currently challenged by an aging population and the fluctuations of an export-driven economy.</background>
	</introduction>
	<geography name="Geography">
		<location name="Location">Northern Europe, bordering the Baltic Sea, Gulf of Bothnia, and Gulf of Finland, between Sweden and Russia</location>
		<geographic-coordinates name="Geographic coordinates">
			<latitude name="Latitude" unit="N">64 00</latitude>
			<longitude name="Longitude" unit="W">26 00</longitude>
		</geographic-coordinates>
		<area name="Area">
			<total name="Total" unit="sq km">338,145</total>
			<land name="Land" unit="sq km">303,815</land>
			<water name="Water" unit="sq km">34,330</water>
		</area>
		<land-boundaries name="Land boundaries">
			<total name="Total" unit="km">2,563</total>
			<border-countries name="Border countries">
				<border-country name="Norway" unit="km">709</border-country>
				<border-country name="Sweden" unit="km">545</border-country>
				<border-country name="Russia" unit="km">1309</border-country>
			</border-countries>
		</land-boundaries>
		<coastline name="Coastline" unit="km">1,250</coastline>
		<maritime-claims name="Maritime claims">
			<territorial-sea name="Territorial sea" unit="nm">12</territorial-sea>
			<contiguous-zone name="Contiguous zone" unit="nm">24</contiguous-zone>
			<continental-shelf name="Continental shelf" unit="nm">200</continental-shelf>
			<exclusive-fishing-zone name="exclusive fishing zone" unit="nm">12</exclusive-fishing-zone>
		</maritime-claims>
		<climate name="Climate">cold temperate; potentially subarctic but comparatively mild because of moderating influence of the North Atlantic Current, Baltic Sea, and more than 60,000 lakes</climate>
		<terrain name="Terrain">mostly low, flat to rolling plains interspersed with lakes and low hills</terrain>
		<elevation name="Elevation">
			<mean-elevation name="Mean elevation" unit="m">164</mean-elevation>
			<lowest-point name="Lowest point">
				<place>Baltic Sea</place>
				<height unit="m">0</height>
			</lowest-point>
			<highest-point name="Highest point">
				<place>Halti</place>
				<height unit="m">1,328</height>
			</highest-point>
		</elevation>
		<natural-resources name="Natural resources">
			<resource>timber</resource>
			<resource>iron ore</resource>
			<resource>copper</resource>
			<resource>lead</resource>
			<resource>zinc</resource>
			<resource>chromite</resource>
			<resource>nickel</resource>
			<resource>gold</resource>
			<resource>silver</resource>
			<resource>limestone</resource>
		</natural-resources>
		<land-use name="Land use">
			<agricultural-land name="Agricultural land" unit="%">7.5</agricultural-land>
			<arable-land name="Arable land" unit="%">7.4</arable-land>
			<permanent-crops name="Permanent crops" unit="%">0</permanent-crops>
			<permanent-pasture name="Permanent pasture" unit="%">0.1</permanent-pasture>
			<forest name="Forest" unit="%">72.9</forest>
			<other name="Other" unit="%">19.6</other>
		</land-use>
		<irrigated-land name="Irrigated land" unit="sq km">690</irrigated-land>
		<population-distribution name="Population distribution">the vast majority of people are found in the south; the northern interior areas remain sparsely poplulated</population-distribution>
		<natural-hazards name="Natural hazards">severe winters in the north</natural-hazards>
		<environment name="Environment">
			<current-issues name="Current issues">limited air pollution in urban centers; some water pollution from industrial wastes, agricultural chemicals; habitat loss threatens wildlife populations</current-issues>
			<international-agreements name="International agreements">
				<party-to name="Party to">
					<agreement>Air Pollution</agreement>
					<agreement>Air Pollution-Nitrogen Oxides</agreement>
					<agreement>Air Pollution-Persistent Organic Pollutants</agreement>
					<agreement>Air Pollution-Sulfur 85</agreement>
					<agreement>Air Pollution-Sulfur 94</agreement>
					<agreement>Air Pollution-Volatile Organic Compounds</agreement>
					<agreement>Antarctic-Environmental Protocol</agreement>
					<agreement>Antarctic-Marine Living Resources</agreement>
					<agreement>Antarctic Treaty</agreement>
					<agreement>Biodiversity</agreement>
					<agreement>Climate Change</agreement>
					<agreement>Climate Change-Kyoto Protocol</agreement>
					<agreement>Desertification</agreement>
					<agreement>Endangered Species</agreement>
					<agreement>Environmental Modification</agreement>
					<agreement>Hazardous Wastes</agreement>
					<agreement>Law of the Sea</agreement>
					<agreement>Marine Dumping</agreement>
					<agreement>Marine Life Conservation</agreement>
					<agreement>Ozone Layer Protection</agreement>
					<agreement>Ship Pollution</agreement>
					<agreement>Tropical Timber 83</agreement>
					<agreement>Tropical Timber 94</agreement>
					<agreement>Wetlands</agreement>
					<agreement>Whaling</agreement>
				</party-to>
				<signed-but-not-ratified name="Signed, but not ratified">none of the selected agreements</signed-but-not-ratified>
			</international-agreements>
		</environment>
	</geography>
	<people-and-society name="People and Society">
		<population name="Population">5,537,364</population>
		<nationality name="Nationality">
			<noun name="Noun">Finn(s)</noun>
			<adjective name="Adjective">Finnish</adjective>
		</nationality>
		<ethnic-groups name="Ethnic groups">
			<group>Finn</group>
			<group>Swede</group>
			<group>Russian</group>
			<group>Estonian</group>
			<group>Romani</group>
			<group>Sami</group>
		</ethnic-groups>
		<languages name="Languages">
			<official name="Official">
				<language>Finnish</language>
				<language>Swedish</language>
			</official>
			<unofficial name="Unofficial">
				<language>Russian</language>
			</unofficial>
		</languages>
		<religions name="Religions">
			<religion>Lutheran</religion>
			<religion>Greek Orthodox</religion>
		</religions>
		<age-structure name="Age structure">
			<years-0-14 name="0-14 years" unit="%">16.44</years-0-14>
			<years-15-24 name="15-24 years" unit="%">11.21</years-15-24>
			<years-25-54 name="25-54 years" unit="%">37.64</years-25-54>
			<years-55-64 name="55-64 years" unit="%">13.19</years-55-64>
			<years-65 name="65 years and over" unit="%">21.51</years-65>
		</age-structure>
		<dependency-ratios name="Dependency ratios">
			<total-dependency-ratio name="Total dependency ratio">57.9</total-dependency-ratio>
			<youth-dependency-ratio name="Youth dependency ratio">25.9</youth-dependency-ratio>
			<elderly-dependency-ratio name="Elderly dependency ratio">32</elderly-dependency-ratio>
			<potential-support-ratio name="Potential support ratio">3.1</potential-support-ratio>
		</dependency-ratios>
		<median-age name="Median age">
			<total name="Total" unit="years">42.6</total>
			<male name="Male" unit="years">41</male>
			<female name="Female" unit="years">44.3</female>
		</median-age>
		<population-growth-rate name="Population growth rate" unit="%">0.33</population-growth-rate>
		<birth-rate name="Birth rate" unit="births/1,000 population">10.7</birth-rate>
		<death-rate name="Death rate" unit="deaths/1,000 population">10.1</death-rate>
		<net-migration-rate name="Net migration rate" unit="migrant(s)/1,000 population">2.8</net-migration-rate>
		<population-distribution name="Population distribution">the vast majority of people are found in the south; the northern interior areas remain sparsely poplulated</population-distribution>
		<urbanization name="Urbanization">
			<urban-population name="Urban population" unit="% of total population">85.4</urban-population>
			<rate-of-urbanization name="Rate of urbanization" unit="% annual rate of change">0.42</rate-of-urbanization>
		</urbanization>
		<major-urban-areas-population name="Major urban areas population">
			<area name="Helsinki" unit="million">1.279</area>
		</major-urban-areas-population>
		<sex-ratio name="Sex ratio">
			<at-birth name="at birth" unit="male(s)/female ">1.05</at-birth>
			<years-0-14 name="0-14 years" unit="male(s)/female ">1.05</years-0-14>
			<years-15-24 name="15-24 years" unit="male(s)/female ">1.05</years-15-24>
			<years-25-54 name="25-54 years" unit="male(s)/female ">1.04</years-25-54>
			<years-55-64 name="55-64 years" unit="male(s)/female ">0.97</years-55-64>
			<years-65 name="65 years and over" unit="male(s)/female ">0.77</years-65>
			<total-population name="total population" unit="male(s)/female ">0.97</total-population>
		</sex-ratio>
		<mothers-mean-age-at-first-birth name="Mother's mean age at first birth" unit="years">28.8</mothers-mean-age-at-first-birth>
		<maternal-mortality-rate name="Maternal mortality rate" unit="deaths/100,000 live births">3</maternal-mortality-rate>
		<infant-mortality-rate name="Infant mortality rate">
			<total name="Total" unit="deaths/1,000 live births">2.5</total>
			<male name="Male" unit="deaths/1,000 live births">2.7</male>
			<female name="Female" unit="deaths/1,000 live births">2.4</female>
		</infant-mortality-rate>
		<life-expectancy-at-birth name="Life expectancy at birth">
			<total-population name="Total population" unit="years">81.1</total-population>
			<male name="Male" unit="years">78.1</male>
			<female name="Female" unit="years">84.2</female>
		</life-expectancy-at-birth>
		<total-fertility-rate name="Total fertility rate" unit="children born/woman">1.75</total-fertility-rate>
		<health-expenditures name="Health expenditures" unit="% of GDP">9.7</health-expenditures>
		<physicians-density name="Physicians density" unit="physicians/1,000 population">3.81</physicians-density>
		<hospital-bed-density name="Hospital bed density" unit="beds/1,000 population">4.4</hospital-bed-density>
		<drinking-water-source name="Drinking water source">
			<improved name="Improved">
				<urban name="Urban" unit="% of population">100</urban>
				<rural name="Rural" unit="% of population">100</rural>
				<total name="Total" unit="% of population">100</total>
			</improved>
			<unimproved name="Unimproved">
				<urban name="Urban" unit="% of population">0</urban>
				<rural name="Rural" unit="% of population">0</rural>
				<total name="Total" unit="% of population">0</total>
			</unimproved>
		</drinking-water-source>
		<sanitation-facility-access name="Sanitation facility access">
			<improved name="Improved">
				<urban name="Urban" unit="% of population">99.4</urban>
				<rural name="Rural" unit="% of population">88</rural>
				<total name="Total" unit="% of population">97.6</total>
			</improved>
			<unimproved name="Unimproved">
				<urban name="Urban" unit="% of population">0.6</urban>
				<rural name="Rural" unit="% of population">12</rural>
				<total name="Total" unit="% of population">2.4</total>
			</unimproved>
		</sanitation-facility-access>
		<HIV-AIDS name="HIV/AIDS">
			<adult-prevalence-rate name="Adult prevalence rate">NA</adult-prevalence-rate>
			<people-living-with-HIV-AIDS name="people living with HIV/AIDS">NA</people-living-with-HIV-AIDS>
			<deaths name="Deaths">NA</deaths>
		</HIV-AIDS>
		<obesity-adult-prevalence-rate name="Obesity - adult prevalence rate" unit="%">22.2</obesity-adult-prevalence-rate>
		<education-expenditures name="Education expenditures" unit="% of GDP">7.1</education-expenditures>
		<school-life-expectancy name="School life expectancy (primary to tertiary education)">
			<total name="Total" unit="years">19</total>
			<male name="Male" unit="years">19</male>
			<female name="Female" unit="years">20</female>
		</school-life-expectancy>
		<unemployment-youth-ages-15-24 name="Unemployment, youth ages 15-24">
			<total name="Total" unit="%">20.1</total>
			<male name="Male" unit="%">20.9</male>
			<female name="Female" unit="%">19.3</female>
		</unemployment-youth-ages-15-24>
	</people-and-society>
	<government name="Government">
		<country-name name="Country name">
			<conventional-long-form name="conventional long form">Republic of Finland</conventional-long-form>
			<conventional-short-form name="conventional long form">Finland</conventional-short-form>
			<etymology name="Etymology">name may derive from the ancient Fenni peoples who are first described as living in northeastern Europe in the first centuries A.D.</etymology>
		</country-name>
		<government-type name="Government type">parliamentary republic</government-type>
		<capital name="Capital">
			<name name="Name">Helsinki</name>
			<geographic-coordinates name="Geographic coordinates">
				<latitude name="Latitude" unit="N">60 10</latitude>
				<longitude name="Longitude" unit="W">24 56</longitude>
			</geographic-coordinates>
			<time-difference name="Time difference">UTC+2</time-difference>
			<daylight-saving-time name="Daylight saving time">+1hr</daylight-saving-time>
		</capital>
		<independence name="Independence">6 December 1917 (from Russia)</independence>
		<national-holiday name="National holiday">Independence Day, 6 December (1917)</national-holiday>
		<constitution name="Constitution">
			<history name="History">previous 1906, 1919; latest drafted 17 June 1997, approved by Parliament 11 June 1999, entered into force 1 March 2000</history>
			<amendments name="Amendments">proposed by Parliament; passage normally requires simple majority vote in two readings in the first parliamentary session and at least two-thirds majority vote in a single reading by the newly elected Parliament; proposals declared "urgent" by five-sixths of Parliament members can be passed by at least two-thirds majority vote in the first parliamentary session only; amended several times, last in 2012 (2016)</amendments>
		</constitution>
		<legal-system name="Legal system">civil law system based on the Swedish model</legal-system>
		<international-law-organization-participation name="International law organization participation">accepts compulsory ICJ jurisdiction with reservations; accepts ICCt jurisdiction</international-law-organization-participation>
		<citizenship name="Citizenship">
			<citizenship-by-birth name="Citizenship by birth">no</citizenship-by-birth>
			<citizenship-by-descent-only name="Citizenship by descent only">at least one parent must be a citizen of Finlandyes</citizenship-by-descent-only>
			<dual-citizenship-recognized name="Dual citizenship recognized">yes</dual-citizenship-recognized>
			<residency-requirement-for-naturalization name="Residency requirement for naturalization">6 years</residency-requirement-for-naturalization>
		</citizenship>
		<suffrage name="Suffrage">18 years of age; universal</suffrage>
		<executive-branch name="Executive branch">
			<chief-of-state name="Chief of state">President Sauli NIINISTO</chief-of-state>
			<head-of-government name="Head of government">Prime Minister Juha SIPILA</head-of-government>
			<cabinet name="Cabinet">Council of State or Valtioneuvosto appointed by the president, responsible to Parliament</cabinet>
			<elections-appointments name="Elections/appointments">president directly elected by absolute majority popular vote in 2 rounds if needed for a 6-year term (eligible for a second term); election last held on 28 January 2018 (next to be held in January 2024); prime minister appointed by Parliament</elections-appointments>
		</executive-branch>
		<legislative-branch name="Legislative branch">
			<description name="Description">unicameral Parliament or Eduskunta (200 seats; 199 members directly elected in single- and multi-seat constituencies by proportional representation vote and 1 member in the province of Aland directly elected by simple majority vote; members serve 4-year terms)</description>
			<elections name="Elections">last held on 14 April 2019 (next to be held on April 2023)</elections>
			<election-results name="Election results">percent of vote by party/coalition - Kesk 21.1%, PS 17.6%, Kok 18.2%, SDP 16.5%, Vihr 8.5%, Vas 7.1%, SFP 4.9%, KD 3.5%, other 2.6%; seats by party/coalition - Kesk 49, PS 38, Kok 37, SDP 34, Vihr 15, Vas 12, SFP 9, KD 5, Aland Coalition 1; composition men 107, women 93, percent of women 46.5%</election-results>
		</legislative-branch>
		<judicial-branch name="Judicial branch">
			<highest-courts name="Highest courts">Supreme Court or Korkein Oikeus (consists of the court president and 18 judges); Supreme Administrative Court (consists of 21 judges, including the court president and organized into 3 chambers); note - Finland has a dual judicial system - courts with civil and criminal jurisdiction and administrative courts with jurisdiction for litigation between individuals and administrative organs of the state and communities</highest-courts>
			<judge-selection-and-term-of-office name="Judge selection and term of office">Supreme Court and Supreme Administrative Court judges appointed by the president of the republic; judges serve until mandatory retirement at age 68</judge-selection-and-term-of-office>
			<subordinate-courts name="Subordinate courts">6 Courts of Appeal; 8 regional administrative courts; 27 district courts; special courts for issues relating to markets, labor, insurance, impeachment, land, tenancy, and water rights</subordinate-courts>
		</judicial-branch>
		<political-parties name="Political parties">
			<party>Aland Coalition</party>
			<party>Center Party or Kesk</party>
			<party>Christian Democrats</party>
			<party>Finns Party</party>
			<party>Green League</party>
			<party>Left Alliance</party>
			<party>National Coalition Party</party>
			<party>Social Democratic Party</party>
			<party>Swedish People's Party</party>
		</political-parties>
		<diplomatic-representation-in-the-US name="Diplomatic representation in the US">
			<chief-of-mission name="Chief-of-mission">Ambassador Kirsti KAUPPI (since 17 September 2015)</chief-of-mission>
			<chancery name="Chancery">3301 Massachusetts Avenue NW, Washington, DC 20008</chancery>
			<telephone name="Telephone">[1] (202) 298-5800</telephone>
			<FAX name="FAX">[1] (202) 298-6030</FAX>
			<consulates-general name="Consulate(s) general">
				<consulate>Los Angeles</consulate>
				<consulate>New York</consulate>
			</consulates-general>
		</diplomatic-representation-in-the-US>
		<diplomatic-representation-from-the-US name="Diplomatic representation from the US">
			<chief-of-mission name="Chief of mission">Ambassador Robert "Bob" Frank PENCE (since 24 May 2018)</chief-of-mission>
			<embassy name="Embassy">Itainen Puistotie 14B, 00140 Helsinki</embassy>
			<mailing-address name="Mailing address">APO AE 09723</mailing-address>
			<telephone name="Telephone">[358] (9) 6162-50</telephone>
			<FAX name="FAX">[358] (9) 6162-5135</FAX>
		</diplomatic-representation-from-the-US>
		<flag-description name="Flag description">white with a blue cross extending to the edges of the flag; the vertical part of the cross is shifted to the hoist side in the style of the Dannebrog (Danish flag); the blue represents the thousands of lakes scattered across the country, while the white is for the snow that covers the land in winter</flag-description>
		<national-symbols name="National symbol(s)">
			<symbol>lion</symbol>
		</national-symbols>
		<national-colors name="National symbol(s)">
			<color>blue</color>
			<color>white</color>
		</national-colors>
		<national-anthem name="National anthem">
			<name name="Name">"Maamme" (Our Land)</name>
			<lyrics-music name="Lyrics/music">Johan Ludvig RUNEBERG/Fredrik PACIUS</lyrics-music>
		</national-anthem>
	</government>
	<economy name="Economy">
		<economy-overview name="Economy overview">Finland has a highly industrialized, largely free-market economy with per capita GDP almost as high as that of Austria and the Netherlands and slightly above that of Germany and Belgium. Trade is important, with exports accounting for over one-third of GDP in recent years. The government is open to, and actively takes steps to attract, foreign direct investment.

Finland is historically competitive in manufacturing, particularly in the wood, metals, engineering, telecommunications, and electronics industries. Finland excels in export of technology as well as promotion of startups in the information and communications technology, gaming, cleantech, and biotechnology sectors. Except for timber and several minerals, Finland depends on imports of raw materials, energy, and some components for manufactured goods. Because of the cold climate, agricultural development is limited to maintaining self-sufficiency in basic products. Forestry, an important export industry, provides a secondary occupation for the rural population.

Finland had been one of the best performing economies within the EU before 2009 and its banks and financial markets avoided the worst of global financial crisis. However, the world slowdown hit exports and domestic demand hard in that year, causing Finland’s economy to contract from 2012 to 2014. The recession affected general government finances and the debt ratio. The economy returned to growth in 2016, posting a 1.9% GDP increase before growing an estimated 3.3% in 2017, supported by a strong increase in investment, private consumption, and net exports. Finnish economists expect GDP to grow a rate of 2-3% in the next few years.

Finland's main challenges will be reducing high labor costs and boosting demand for its exports. In June 2016, the government enacted a Competitiveness Pact aimed at reducing labor costs, increasing hours worked, and introducing more flexibility into the wage bargaining system. As a result, wage growth was nearly flat in 2017. The Government was also seeking to reform the health care system and social services. In the long term, Finland must address a rapidly aging population and decreasing productivity in traditional industries that threaten competitiveness, fiscal sustainability, and economic growth.</economy-overview>
		<gdp name="GDP">
			<purchasing-power-parity name="Purchasing power parity" unit="billion dollars">244.9</purchasing-power-parity>
			<official-exchange-rate name="Official exchange rate" unit="biillion dollars">252.8</official-exchange-rate>
			<real-growth-rate name="Real growth rate" unit="%">2.8</real-growth-rate>
			<per-capita name="Per capita (PPP)" unit="$">44,500</per-capita>
			<gross-national-saving name="Gross national saving" unit="% of GDP">23.3</gross-national-saving>
			<composition-by-end-use name="Composition, by end use">
				<household-consumption name="Household consumption" unit="%">54.4</household-consumption>
				<government-consumption name="Government consumption" unit="%">22.9</government-consumption>
				<investment-in-fixed-capital name="Investment in fixed capital" unit="%">22.1</investment-in-fixed-capital>
				<investment-in-inventories name="Investment in inventories" unit="%">0.4</investment-in-inventories>
				<exports-of-goods-and-services name="Exports of goods and services" unit="%">38.5</exports-of-goods-and-services>
				<imports-of-goods-and-services name="Imports of goods and services" unit="%">-38.2</imports-of-goods-and-services>
			</composition-by-end-use>
			<composition-by-sector-of-origin name="Composition, by sector of origin">
				<agriculture name="Agriculture" unit="%">2.7</agriculture>
				<industry name="Industry" unit="%">28.2</industry>
				<services name="Services" unit="%">69.1</services>
			</composition-by-sector-of-origin>
		</gdp>
		<agriculture-products name="Agriculture products">
			<product>barley</product>
			<product>wheat</product>
			<product>sugar beets</product>
			<product>potatoes</product>
			<product>dairy cattle</product>
			<product>fish</product>
		</agriculture-products>
		<industries name="Industries">
			<industry>metals and metal products</industry>
			<industry>electronics</industry>
			<industry>machinery and scientific instruments</industry>
			<industry>shipbuilding</industry>
			<industry>pulp and paper</industry>
			<industry>foodstuffs</industry>
			<industry>chemicals</industry>
			<industry>textiles</industry>
			<industry>clothing</industry>
		</industries>
		<industrial-production-growth-rate name="Industrial production growth rate" unit="%">6.2</industrial-production-growth-rate>
		<labor-force name="Labor force" unit="million">2.473</labor-force>
		<unemployment-rate name="Unemployment rate" unit="%">8.5</unemployment-rate>
		<household-income-or-consumption-by-percentage-share name="Household income or consumption by percentage share">
			<lowest-10 name="Lowest 10%" unit="%">6.7</lowest-10>
			<highest-10 name="Highest 10%" unit="%">45.2</highest-10>
		</household-income-or-consumption-by-percentage-share>
		<distribution-of-family-income-Gini-index name="Distribution of family income - Gini index">27.2</distribution-of-family-income-Gini-index>
		<budget name="Budget">
			<revenues name="Revenues" unit="billion dollars">134.2</revenues>
			<expenditures name="Expenditures" unit="billion dollars">135.6</expenditures>
		</budget>
		<taxes-and-other-revenues name="Taxes and other revenues" unit="% of GDP">53.1</taxes-and-other-revenues>
		<budget-surplus-or-deficit name="Budget surplus (+) or deficit (-)" unit="% of GDP">-0.6</budget-surplus-or-deficit>
		<public-debt name="Public debt" unit="% of GDP">61.3</public-debt>
		<fiscal-year name="Fiscal year">calendar year</fiscal-year>
		<inflation-rate-consumer-prices name="Inflation rate (consumer prices)" unit="%">0.8</inflation-rate-consumer-prices>
		<central-bank-discount-rate name="Central bank discount rate" unit="%">1.25</central-bank-discount-rate>
		<commercial-bank-prime-lending-rate name="Commercial bank prime lending rate" unit="%">1.61</commercial-bank-prime-lending-rate>
		<stock-of-narrow-money name="Stock of narrow money" unit="billion dollars">152.6</stock-of-narrow-money>
		<stock-of-broad-money name="Stock of broad money" unit="billion dollars">152.6</stock-of-broad-money>
		<stock-of-domestic-credit name="Stock of domestic credit" unit="billion dollars">323.9</stock-of-domestic-credit>
		<market-value-of-publicly-traded-shares name="Market value of publicly traded shares" unit="billion dollars">231</market-value-of-publicly-traded-shares>
		<current-account-balance name="Current account balance" unit="billion dollars">1.806</current-account-balance>
		<exports name="Exports" unit="billion dollars">67.73</exports>
		<export-partners name="Exports partners">
			<partner name="Germany" unit="%">14.2</partner>
			<partner name="Sweden" unit="%">10.1</partner>
			<partner name="US" unit="%">7</partner>
			<partner name="Netherlands" unit="%">6.8</partner>
			<partner name="China" unit="%">5.7</partner>
			<partner name="Russia" unit="%">5.7</partner>
			<partner name="UK" unit="%">4.5</partner>
		</export-partners>
		<export-commodities name="Exports commodities">
			<commodity>electrical and optical equipment</commodity>
			<commodity>machinery</commodity>
			<commodity>transport equipmen</commodity>
			<commodity>paper and pulp</commodity>
			<commodity>chemicals</commodity>
			<commodity>basic metals</commodity>
			<commodity>timber</commodity>
		</export-commodities>
		<imports name="Imports" unit="billion dollars">65.26</imports>
		<import-commodities name="Imports commodities">
			<commodity>foodstuffs</commodity>
			<commodity>petroleum and petroleum products</commodity>
			<commodity>chemicals</commodity>
			<commodity>transport equipment</commodity>
			<commodity>iron and steel</commodity>
			<commodity>machinery</commodity>
			<commodity>computers</commodity>
			<commodity>electronic industry products</commodity>
			<commodity>textile yarn and fabrics</commodity>
			<commodity>grains</commodity>
		</import-commodities>
		<import-partners name="Import partners">
			<partner name="Germany" unit="%">17.7</partner>
			<partner name="Sweden" unit="%">15.8</partner>
			<partner name="Russia" unit="%">13.1</partner>
			<partner name="Netherlands" unit="%">8.7</partner>
		</import-partners>
		<reserves-of-foreign-exchange-and-gold name="Reserves of foreign exchange and gold" unit="billion dollars">10.51</reserves-of-foreign-exchange-and-gold>
		<debt-external name="Debt external" unit="billion dollars">150.6</debt-external>
		<stock-of-direct-foreign-investment name="Stock of direct foreign investment">
			<at-home name="At home" unit="billion dollars">135.2</at-home>
			<abroad name="Abroad" unit="billionn dollars">185.6</abroad>
		</stock-of-direct-foreign-investment>
		<exchange-rates name="Exchange rates" unit="euros (EUR) per US dollar">0.885</exchange-rates>
	</economy>
	<energy name="Energy">
		<electricity name="Electricity">
			<access name="Access" unit="%">100</access>
			<production name="Production" unit="billion kWh">66.54</production>
			<consumption name="Consumption" unit="billion kWh">82.79</consumption>
			<exports name="Exports" unit="billion kWh">3.159</exports>
			<imports name="Imports" unit="billion kWh">22.11</imports>
			<installed-generating-capacity name="Installed generating capacity" unit="million kW">16.27</installed-generating-capacity>
			<from-fossil-fuels name="From fossil fuels" unit="%">41</from-fossil-fuels>
			<from-nuclear-fuels name="From nuclear fuels" unit="%">17</from-nuclear-fuels>
			<from-hydroelectric-plants name="From hydroelectric plants" unit="%">20</from-hydroelectric-plants>
			<from-other-renewable-sources name="from other renewable sources" unit="%">23</from-other-renewable-sources>
		</electricity>
		<crude-oil name="Crude oil">
			<production name="Production" unit="million bbl/day">0</production>
			<exports name="Exports" unit="million bbl/day">0</exports>
			<imports name="Imports" unit="bbl/day">236,700</imports>
			<proved-reserves name="Proved reserves" unit="billion bbl">0</proved-reserves>
		</crude-oil>
		<refined-petroleum-products name="Refined petroleum products">
			<production name="Production" unit="million bbl/day">310,600</production>
			<consumption name="Consumption" unit="million bbl/day">217,100</consumption>
			<exports name="Exports" unit="million bbl/day">166,200</exports>
			<imports name="Imports" unit="bbl/day">122,200</imports>
		</refined-petroleum-products>
		<natural-gas name="Natural gas">
			<production name="Production" unit="cu m">0</production>
			<consumption name="Consumption" unit="million cu m">2.35</consumption>
			<exports name="Exports" unit="million cu m">4</exports>
			<imports name="Imports" unit="million cu m">2.322</imports>
			<proved-reserves name="Proved reserves" unit="trillion cu m">NA</proved-reserves>
		</natural-gas>
		<emissions name="Carbon dioxide emissions from consumption of energy" unit="million Mt">46.01</emissions>
	</energy>
	<communications name="Communications">
		<telephones name="Telephones">
			<fixed-lines name="Fixed lines">
				<total-subscriptions name="Total subscriptions">378,200</total-subscriptions>
				<subscriptions-per-100-inhabitants name="Subscriptions per 100 inhabitants">7</subscriptions-per-100-inhabitants>
			</fixed-lines>
			<mobile-cellular name="Mobile Cellular">
				<total-subscriptions name="Total subscriptions">7,307,800</total-subscriptions>
				<subscriptions-per-100-inhabitants name="Subscriptions per 100 inhabitants">132</subscriptions-per-100-inhabitants>
			</mobile-cellular>
			<telephone-system name="Telephone system">
				<general-assessment name="General assessment">modern system with excellent service; one of the most progressive in Europe; one of the highest broadband and mobile penetrations rates in the region; forefront in testing 5G networks; for 2025 and 2030 FttP (fiber to the home) and DOCSIS3.1 (new generation of cable services for high speed connections) technologies (2017)</general-assessment>
				<domestic name="Domestic">digital fiber-optic, fixed-line 7 per 100 subscription; 132 per 100 mobile-cellular; network and an extensive mobile-cellular network provide domestic needs (2017)</domestic>
				<international name="International">country code - 358; submarine cables provide links to Estonia and Sweden; satellite earth stations - access to Intelsat transmission service via a Swedish satellite earth station, 1 Inmarsat (Atlantic and Indian Ocean regions); note - Finland shares the Inmarsat earth station with the other Nordic countries (Denmark, Iceland, Norway, and Sweden) (2015)</international>
			</telephone-system>
		</telephones>
		<broadcast-media name="Broadcast media">a mix of 3 publicly operated TV stations and numerous privately owned TV stations; several free and special-interest pay-TV channels; cable and satellite multi-channel subscription services are available; all TV signals are broadcast digitally; Internet television, such as Netflix and others, is available; public broadcasting maintains a network of 13 national and 25 regional radio stations; a large number of private radio broadcasters and access to Internet radio (2017)</broadcast-media>
		<internet-country-code name="Internet country code">.finote - Aland Islands assigned .ax</internet-country-code>
		<internet-users name="Internet users">
			<total name="Total">4,822,132</total>
			<percent-of-population name="Percent of population" unit="%">87.7</percent-of-population>
		</internet-users>
		<broadband-fixed-subscriptions name="Broadband fixed subscriptions">
			<total name="Total">1,709,400</total>
			<subscriptions-per-100-inhabitants name="Subscriptions per 100 inhabitants">31</subscriptions-per-100-inhabitants>
		</broadband-fixed-subscriptions>
	</communications>
	<transportation name="Transportation">
		<national-air-transport-system name="National air transport system">
			<number-of-registered-air-carriers name="Number of registered air carriers">3</number-of-registered-air-carriers>
			<inventory-of-registered-aircraft-operated-by-air-carriers name="Inventory of registered aircraft operated by air carriers">73</inventory-of-registered-aircraft-operated-by-air-carriers>
			<annual-passenger-traffic-on-registered-air-carriers name="Annual passenger traffic on registered air carriers">9,972,333</annual-passenger-traffic-on-registered-air-carriers>
			<annual-freight-traffic-on-registered-air-carriers name="Annual freight traffic on registered air carriers" unit="mt-km">713.484</annual-freight-traffic-on-registered-air-carriers>
		</national-air-transport-system>
		<civil-aircraft-registration-country-code-prefix name="Civil aircraft registration country code prefix">OH (2016)</civil-aircraft-registration-country-code-prefix>
		<airports name="Airports">148</airports>
		<airports-with-paved-runways name="Airports with paved runways">
			<total name="Total">74</total>
			<m-over-3047 name="Over 3,047 m">3</m-over-3047>
			<m-2438-to-3047 name="2,438 to 3,047 m">26</m-2438-to-3047>
			<m-1524-to-2437 name="1,524 to 2,437 m">10</m-1524-to-2437>
			<m-914-to-1523 name="914 to 1,523 m">21</m-914-to-1523>
			<m-under-914 name="under 914 m">14</m-under-914>
		</airports-with-paved-runways>
		<airports-with-unpaved-runways name="Airports with unpaved runways">
			<total name="Total">74</total>
			<m-914-to-1523 name="914 to 1,523 m">3</m-914-to-1523>
			<m-under-914 name="under 914 m">71</m-under-914>
		</airports-with-unpaved-runways>
		<pipelines name="Pipelines" unit="km gas transmission pipes">1288</pipelines>
		<railways name="Railways">
			<total name="Total" unit="km">5,926</total>
			<broad-gauge name="Broad gauge" unit="km">5,926</broad-gauge>
		</railways>
		<roadways name="Roadways">
			<total name="Total" unit="km">454,000</total>
			<paved name="Paved" unit="km">50,000</paved>
			<unpaved name="Unpaved" unit="km">28,000</unpaved>
		</roadways>
		<waterways name="Waterways" unit="km">8,000</waterways>
		<merchant-marine name="Merchant marine">
			<total name="Total">259</total>
			<by-type name="By type">
				<type name="Bulk carrier">7</type>
				<type name="Container ship">1</type>
				<type name="General cargo">88</type>
				<type name="Oil tanker">4</type>
				<type name="Other">159</type>
			</by-type>
		</merchant-marine>
		<ports-and-terminals name="Ports and terminals">
			<major-seaports name="Major seaport(s)">
				<seaport>Helsinki</seaport>
				<seaport>Kotka</seaport>
				<seaport>Naantali</seaport>
				<seaport>Porvoo</seaport>
				<seaport>Raahe</seaport>
				<seaport>Rauma</seaport>
			</major-seaports>
		</ports-and-terminals>
	</transportation>
	<military-and-security name="Military and Security">
		<military-expenditures name="Military expenditures" unit="% of GDP">1.29</military-expenditures>
		<military-branches name="Military branches">
			<forces name="Finnish Defense Forces">
				<force>Army (Puolustusvoimat)</force>
				<force>Navy (Merivoimat, includes Coastal Defense Forces)</force>
				<force>Air Force (Ilmavoimat)</force>
			</forces>
		</military-branches>
		<service-age-and-obligation name="Military service age and obligation">all Finnish men are called-up for military service the year they turn 18; at 18, women may volunteer for military service; service obligation 6-12 months; individuals enter the reserve upon completing their initial obligation; military obligation to age 60 (2016)</service-age-and-obligation>
	</military-and-security>
	<transnational-issues name="Transnational Issues">
		<international-disputes name="International disputes">various groups in Finland advocate restoration of Karelia and other areas ceded to the former Soviet Union, but the Finnish Government asserts no territorial demands</international-disputes>
		<refugees-and-internally-displaced-persons name="Refugees and internally displaced persons">
			<refugees name="Refugees">
				<origin-country name="Iraq">7,849</origin-country>
			</refugees>
			<stateless-persons name="Stateless persons">2,749</stateless-persons>
		</refugees-and-internally-displaced-persons>
	</transnational-issues>
</country>
