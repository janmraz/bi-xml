<?xml version="1.0"?>

<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
xmlns:fo="http://www.w3.org/1999/XSL/Format">

<xsl:template match="/">
  <fo:root font-family="Times New Roman, serif" font-size="11pt" language="en" hyphenate="true">
    <fo:layout-master-set>
      <fo:simple-page-master master-name="my-page"
                             page-height="297mm"
                             page-width="180mm"
                             margin="1in">
        <fo:region-body margin-bottom="10mm"/>
        <fo:region-after extent="5mm"/>
      </fo:simple-page-master>
    </fo:layout-master-set>

    <fo:page-sequence master-reference="my-page">
      <fo:static-content flow-name="xsl-region-after">
        <fo:block>
          <xsl:text>Page </xsl:text>
          <fo:page-number/>
        </fo:block>
      </fo:static-content>
      <fo:flow flow-name="xsl-region-body">
        <fo:block font-family="serif" space-after="22pt" keep-with-next="always" text-align="center" line-height="32pt" font-size="32pt" >
               <xsl:text>BI-XML Project</xsl:text>
        </fo:block>
        <xsl:apply-templates mode="menu" />
        <xsl:apply-templates/>
      </fo:flow>
    </fo:page-sequence>
  </fo:root>
</xsl:template>

<xsl:template match="countries">
    <fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="countries" mode="menu">
    <fo:block>
        <fo:block font-family="serif" space-after="10pt" keep-with-next="always" line-height="20pt" font-size="20pt" >
               <xsl:text>Countries</xsl:text>
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="country">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <fo:basic-link internal-destination="{generate-id(.)}">
	                            <xsl:value-of select="@name" />
                            </fo:basic-link>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="country">
    <fo:block id="{generate-id(.)}" >
        <fo:block font-family="serif" space-after="18pt" keep-with-next="always" line-height="28pt" font-size="24pt" >
                <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="images">
    <fo:block>
            <xsl:apply-templates />
    </fo:block>
</xsl:template>


<xsl:template match="image">
    <fo:external-graphic width="100%"
                        content-height="100%"
                        content-width="scale-to-fit"
                        scaling="uniform">
        <xsl:attribute name="src">
           <xsl:text>../resources/</xsl:text>
           <xsl:value-of select="."/>
        </xsl:attribute>
    </fo:external-graphic>
</xsl:template>

<xsl:template match="introduction">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="background" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="geography">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="location">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="geographic-coordinates">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="latitude/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="latitude" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="latitude/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="longitude/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="longitude" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="longitude/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="area">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="water/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="water" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="water/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="land-boundaries">
<fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
        </fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
            <xsl:value-of select="border-countries/@name" />
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
        <xsl:for-each select="border-countries/border-country">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                    <fo:block><xsl:text>---</xsl:text></fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()">
                    <fo:block>
                            <xsl:value-of select="@name"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="."/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="@unit"/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="coastline">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="maritime-claims">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="territorial-sea/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="territorial-sea" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="territorial-sea/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="exclusive-economic-zone/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exclusive-economic-zone" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exclusive-economic-zone/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="continental-shelf/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="continental-shelf" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="continental-shelf-shelfer/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="climate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="terrain">
        <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="elevation">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="mean-elevation/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mean-elevation" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mean-elevation/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="lowest-point/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-point/place" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-point/height/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="highest-point/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-point/place" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-point/height/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="land-use">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="agricultural-land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agricultural-land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agricultural-land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="arable-land/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="arable-land" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="arable-land/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="permanent-crops/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-crops" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-crops/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="permanent-pasture/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-pasture" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="permanent-pasture/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="forest/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="forest" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="forest/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="other/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="other" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="other/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="natural-resources">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" ><xsl:value-of select="@name" /></fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
        <xsl:for-each select="resource">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                    <fo:block><xsl:text>---</xsl:text></fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()">
                    <fo:block>
                        <xsl:value-of select="."/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="irrigated-land">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="natural-hazards">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="environment">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="current-issues">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="international-agreements">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
             <fo:block>
                <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                    <xsl:value-of select="party-to/@name" />
                </fo:block>
                <fo:list-block padding="1pt" space-after="12pt">
                <xsl:for-each select="party-to/agreement">
                    <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                            <fo:block><xsl:text>---</xsl:text></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                                    <xsl:value-of select="."/>
                            </fo:block>
                        </fo:list-item-body>
                    </fo:list-item>
                </xsl:for-each>
                </fo:list-block>
            </fo:block>
            <fo:block>
                <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                    <xsl:value-of select="signed-but-not-ratified/@name" />
                </fo:block>
                <fo:block>
                    <xsl:value-of select="signed-but-not-ratified"/>
                </fo:block>
            </fo:block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="people-and-society">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="population">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="nationality">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="noun/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="noun" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="adjective/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adjective" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="population-distribution">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="ethnic-groups">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="group">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="languages">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
             <fo:block>
                <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                    <xsl:value-of select="official/@name" />
                </fo:block>
                <fo:list-block padding="1pt" space-after="12pt">
                <xsl:for-each select="official/language">
                    <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                            <fo:block><xsl:text>---</xsl:text></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                                    <xsl:value-of select="."/>
                            </fo:block>
                        </fo:list-item-body>
                    </fo:list-item>
                </xsl:for-each>
                </fo:list-block>
            </fo:block>
            <fo:block>
                <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                    <xsl:value-of select="unofficial/@name" />
                </fo:block>
                <fo:list-block padding="1pt" space-after="12pt">
                <xsl:for-each select="unofficial/language">
                    <fo:list-item>
                        <fo:list-item-label end-indent="label-end()">
                            <fo:block><xsl:text>---</xsl:text></fo:block>
                        </fo:list-item-label>
                        <fo:list-item-body start-indent="body-start()">
                            <fo:block>
                                    <xsl:value-of select="."/>
                            </fo:block>
                        </fo:list-item-body>
                    </fo:list-item>
                </xsl:for-each>
                </fo:list-block>
            </fo:block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="religions">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="religion">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="age-structure">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-0-14/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-0-14" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-0-14/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-15-24/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-15-24" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-15-24/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-25-54/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-25-54" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-25-54/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-55-64/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-55-64" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-55-64/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-65/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-65" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-65/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="dependency-ratios">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="youth-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="youth-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elderly-dependency-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elderly-dependency-ratio" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="potential-support-ratio/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="potential-support-ratio" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="median-age">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="population-growth-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="birth-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="death-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="net-migration-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="urbanization">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="urban-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="urban-population" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="urban-population/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="rate-of-urbanization/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="rate-of-urbanization" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="rate-of-urbanization/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="major-urban-areas-population">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="area">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="@name"/>
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="."/>
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="sex-ratio">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="at-birth/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="at-birth" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="at-birth/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-0-14/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-0-14" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-0-14/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-15-24/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-15-24" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-15-24/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-25-54/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-25-54" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-25-54/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-55-64/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-55-64" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-55-64/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="years-65/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-65" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="years-65/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="total-population/@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="total-population" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="total-population/@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="mothers-mean-age-at-first-birth">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="maternal-mortality-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="infant-mortality-rate">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="life-expectancy-at-birth">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-population" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total-population/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="total-fertility-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="health-expenditures">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="physicians-density">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="hospital-bed-density">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="drinking-water-source">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="improved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="improved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total/@unit" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="unimproved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unimproved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="sanitation-facility-access">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="improved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="improved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="improved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="improved/total/@unit" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="unimproved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unimproved/urban/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/urban/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/rural/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/rural/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unimproved/total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unimproved/total/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="HIV-AIDS">
  <fo:block>
    <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
        <xsl:value-of select="@name" />
    </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="adult-prevalence-rate/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adult-prevalence-rate" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="adult-prevalence-rate/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="people-living-with-HIV-AIDS/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="deaths/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="deaths" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="deaths/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="education-expenditures">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="obesity-adult-prevalence-rate">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="school-life-expectancy">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="unemployment-youth-ages-15-24">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="male/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="male/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="female/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="female/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="government">
    <fo:block>
      <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="country-name">
  <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="conventional-long-form/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="conventional-long-form" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="conventional-short-form/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="conventional-short-form" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="etymology/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="etymology" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="government-type">
    <fo:block>
      <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
          <xsl:value-of select="@name" />
      </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="capital">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="independence">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="national-holiday">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="constitution">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                <xsl:value-of select="history/@name" />
            </fo:block>
            <xsl:value-of select="history" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
                <xsl:value-of select="amendments/@name" />
            </fo:block>
            <xsl:value-of select="amendments" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="legal-system">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="international-law-organization-participation">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="citizenship">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="citizenship-by-birth/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="citizenship-by-birth" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="citizenship-by-descent-only/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="citizenship-by-descent-only" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="dual-citizenship-recognized/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="dual-citizenship-recognized" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="residency-requirement-for-naturalization/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="residency-requirement-for-naturalization" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="suffrage">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="executive-branch">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="chief-of-state/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="chief-of-state" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="head-of-government/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="head-of-government" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="cabinet/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="cabinet" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elections-appointments/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elections-appointments" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="legislative-branch">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="description/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="description" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="elections/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="elections" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="election-results/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="election-results" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="judicial-branch">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="highest-courts/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-courts" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="judge-selection-and-term-of-office/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="judge-selection-and-term-of-office" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="subordinate-courts/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="subordinate-courts" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="political-parties">
      <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
        <xsl:for-each select="party">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                    <fo:block><xsl:text>---</xsl:text></fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()">
                    <fo:block>
                        <xsl:value-of select="."/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="diplomatic-representation-in-the-US">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
          <xsl:value-of select="chief-of-mission/@name" />
          <xsl:text> - </xsl:text>
          <xsl:value-of select="chief-of-mission" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="chancery/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="chancery" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="telephone/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="telephone" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="FAX/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="FAX" />
        </fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
            <xsl:value-of select="border-countries/@name" />
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
        <xsl:for-each select="consulates-general/consulate">
            <fo:list-item>
                <fo:list-item-label end-indent="label-end()">
                    <fo:block><xsl:text>---</xsl:text></fo:block>
                </fo:list-item-label>
                <fo:list-item-body start-indent="body-start()">
                    <fo:block>
                        <xsl:value-of select="."/>
                    </fo:block>
                </fo:list-item-body>
            </fo:list-item>
        </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="diplomatic-representation-from-the-US">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
          <xsl:value-of select="chief-of-mission/@name" />
          <xsl:text> - </xsl:text>
          <xsl:value-of select="chief-of-mission" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="embassy/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="embassy" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="telephone/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="telephone" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="FAX/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="FAX" />
          <xsl:text>, </xsl:text>
          <xsl:value-of select="mailing-address/@name" />
          <xsl:text> </xsl:text>
          <xsl:value-of select="mailing-address" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="flag-description">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="national-symbols">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="symbol">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="national-colors">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="color">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="national-anthem">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="name/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="name" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="lyrics-music/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lyrics-music" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="economy">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="economy-overview">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="gdp">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="purchasing-power-parity">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="official-exchange-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="real-growth-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="per-capita">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="gross-national-saving">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="purchasing-power-parity">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="composition-by-end-use">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="household-consumption/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="household-consumption" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="household-consumption/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="government-consumption/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="government-consumption" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="government-consumption/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-fixed-capita/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="investment-in-inventories/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-inventories" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="investment-in-inventories/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="exports-of-goods-and-services/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="imports-of-goods-and-services/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="composition-by-sector-of-origin">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="agriculture/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agriculture" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="agriculture/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="industry/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="industry" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="industry/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="services/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="services" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="services/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="agriculture-products">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="product">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="industries">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="industry">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="."/>
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="industrial-production-growth-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="labor-force">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="unemployment-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="population-below-poverty-line">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="household-income-or-consumption-by-percentage-share">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="lowest-10/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="lowest-10" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="lowest-10/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="highest-10/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="highest-10" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="highest-10/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="distribution-of-family-income-Gini-index">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="budget">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="revenues/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="revenues" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="revenues/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="expenditures/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="expenditures" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="expenditures/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="taxes-and-other-revenues">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="budget-surplus-or-deficit">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="public-debt">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="fiscal-year">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="inflation-rate-consumer-prices">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="central-bank-discount-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="commercial-bank-prime-lending-rate">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="stock-of-narrow-money">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="stock-of-broad-money">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="stock-of-domestic-credit">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="market-value-of-publicly-traded-shares">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="current-account-balance">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="exports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="export-partners">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="partner">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="." />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="export-commodities">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="commodity">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="imports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="import-commodities">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="commodity">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="import-partners">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="partner">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="." />
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="@unit" />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="reserves-of-foreign-exchange-and-gold">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="debt-external">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="stock-of-direct-foreign-investment">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="at-home/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="at-home" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="at-home/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="abroad/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="abroad" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="abroad/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="exchange-rates">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="energy">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="electricity">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="access">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="production">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="consumption">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="installed-generating-capacity">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="from-fossil-fuels">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="from-nuclear-fuels">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="from-hydroelectric-plants">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="from-other-renewable-sources">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="crude-oil">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="refined-petroleum-products">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="natural-gas">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="proved-reserves">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="20pt" font-size="19pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="emissions">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="communications">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="telephones">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="fixed-lines/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="fixed-lines/total-subscriptions/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="fixed-lines/total-subscriptions" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="fixed-lines/subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="fixed-lines/subscriptions-per-100-inhabitants" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="mobile-cellular/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="mobile-cellular/total-subscriptions/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mobile-cellular/total-subscriptions" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="mobile-cellular/subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="mobile-cellular/subscriptions-per-100-inhabitants" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="telephone-system/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="telephone-system/general-assessment/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/general-assessment" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="telephone-system/domestic/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/domestic" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="telephone-system/international/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="telephone-system/international" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="broadcast-media">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="internet-country-code">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="internet-users">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="percent-of-population/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="percent-of-population" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="broadband-fixed-subscriptions">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="subscriptions-per-100-inhabitants/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="subscriptions-per-100-inhabitants" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="transportation">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="national-air-transport-system">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="number-of-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="number-of-registered-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="inventory-of-registered-aircraft-operated-by-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="inventory-of-registered-aircraft-operated-by-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="annual-passenger-traffic-on-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-passenger-traffic-on-registered-air-carriers" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers/@name" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="annual-freight-traffic-on-registered-air-carriers/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="civil-aircraft-registration-country-code-prefix">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="airports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="airports-with-paved-runways">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-over-3047/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-over-3047" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-2438-to-3047/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-2438-to-3047" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-1524-to-2437/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-1524-to-2437" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-914-to-1523/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-914-to-1523" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-under-914/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-under-914" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="airports-with-unpaved-runways">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-1524-to-2437/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-1524-to-2437" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-914-to-1523/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-914-to-1523" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="m-under-914/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="m-under-914" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="heliports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="pipelines">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="railways">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="standard-gauge/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="standard-gauge" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="standard-gauge/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="roadways">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="total" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="total/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="paved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="paved" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="paved/@unit" />
            <xsl:text>, </xsl:text>
            <xsl:value-of select="unpaved/@name" />
            <xsl:text> - </xsl:text>
            <xsl:value-of select="unpaved" />
            <xsl:text> </xsl:text>
            <xsl:value-of select="unpaved/@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="waterways">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="merchant-marine">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="total" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="ports-and-terminals">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="major-seaports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="seaport">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="seaport">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="terminal">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="container-ports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="port">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="LNG-terminals">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="terminal">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="river-and-lake-ports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="port">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="dry-bulk-cargo-ports">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="port">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
            </fo:list-block>
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="military-and-security">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="military-expenditures">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
            <xsl:text> </xsl:text>
            <xsl:value-of select="@unit" />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="military-branches">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
            <xsl:value-of select="forces" />
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="forces/force">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="service-age-and-obligation">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="transnational-issues">
    <fo:block>
        <fo:block font-family="serif" space-after="14pt" keep-with-next="always" line-height="24pt" font-size="21pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block>
            <xsl:apply-templates />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="international-disputes">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>

<xsl:template match="refugees-and-internally-displaced-persons">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="16pt" font-size="14pt" >
            <xsl:value-of select="refugees/@name" />
        </fo:block>
        <fo:list-block padding="1pt" space-after="12pt">
            <xsl:for-each select="refugees/origin-country">
                <fo:list-item>
                    <fo:list-item-label end-indent="label-end()">
                        <fo:block><xsl:text>---</xsl:text></fo:block>
                    </fo:list-item-label>
                    <fo:list-item-body start-indent="body-start()">
                        <fo:block>
                            <xsl:value-of select="@name" />
                            <xsl:text> - </xsl:text>
                            <xsl:value-of select="." />
                        </fo:block>
                    </fo:list-item-body>
                </fo:list-item>
            </xsl:for-each>
        </fo:list-block>
    </fo:block>
</xsl:template>

<xsl:template match="illicit-drugs">
    <fo:block>
        <fo:block font-family="serif" space-after="12pt" keep-with-next="always" line-height="19pt" font-size="16pt" >
            <xsl:value-of select="@name" />
        </fo:block>
        <fo:block space-after="18pt">
            <xsl:value-of select="." />
        </fo:block>
    </fo:block>
</xsl:template>


</xsl:stylesheet>
